import list from '../data.json'
function createUserList() {
  return list
}
export default [
  {
    url: '/system/shipping/list', // 请求地址
    method: 'post',
    response: () => {
      const checkUser = createUserList
      // 返回成功信息
      return { code: 200, data: { checkUser } }
    }
  }
]
