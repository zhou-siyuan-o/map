import { defineConfig } from "vite";
import { viteMockServe } from 'vite-plugin-mock'
import vue from "@vitejs/plugin-vue";
export default defineConfig(({ command }) => {
    return {
        plugins: [vue(),
        viteMockServe({
            mockPath: 'mock',
            localEnabled: command === 'serve'
        })
        ],
    }
})