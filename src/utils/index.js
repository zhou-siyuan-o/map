// 防抖：一直触发，直到停止触发 0.5s 后，执行函数
export const debounce = (fn, wait = 500) => {
  let timer = null
  return function () {
    timer && clearTimeout(timer)

    timer = setTimeout(() => {
      fn.apply(this, arguments)
    }, wait)
  }
}

// 节流：1s 内，无论触发多少次，只执行一次
export const throttle = (fn, wait = 1000) => {
  let timer = null
  return function () {
    if (!timer) {
      timer = setTimeout(() => {
        fn.apply(this, arguments)
        timer = null
      }, wait)
    }
  }
}
