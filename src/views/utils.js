let markersList = []
let beforeZoom = 10
let changeSize = 8

// 缩放地图事件
const zoomEvent = () => {
  mapRef.on('zoomstart', () => {
    beforeZoom = mapRef.getZoom()
  })
  mapRef.on('zoomend', () => {
    mapZoom()
  })
}

// 点击弹窗外部隐藏弹窗
const hideMask = (e) => {
  let mask = document.querySelector('.amap-info-contentContainer')
  if (mask) {
    if (!mask.contains(e.target)) {
      closeInfo()
    }
  }
}

// 绘制地图 mark
const addPoint = (size = 1) => {
  let newInfo =
    size > changeSize
      ? new AMap.InfoWindow({ offset: new AMap.Pixel(0, -35) })
      : new AMap.InfoWindow({ offset: new AMap.Pixel(5, -35) })
  infoWindow = newInfo

  fArr.value.forEach((item) => {
    let markObj = {
      position: item.lnglat,
      map: mapRef,
      content: size > changeSize ? '' : _renderClusterMarker({ count: 1 })
    }
    const marker = new AMap.Marker(markObj)

    marker.content = getMarkContent(item)

    marker.on('click', (e) => {
      let result = e.target.content.match(/span>[\u4e00-\u9fa5]+</g)
      if (result.length >= 6 && result.length < 8) {
        result.splice(-4)
      } else if (result.length >= 8) {
        result.splice(-5)
      } else {
        result.splice(-1)
      }
      inputValue.value = result.map((item) => item.match(/[\u4e00-\u9fa5]+/)[0])
      drawBounds()
      infoWindow.setContent(e.target.content)
      infoWindow.open(mapRef, e.target.getPosition())
    })
    markersList.push(marker)
  })

  _fArr.forEach((item) => {
    let markObj = {
      position: item.lnglat,
      map: mapRef,
      content: size > changeSize ? '' : _renderClusterMarker({ count: 1 })
    }
    const marker = new AMap.Marker(markObj)

    marker.content = getMarkContent(item)

    marker.on('click', (e) => {
      let result = e.target.content.match(/span>[\u4e00-\u9fa5]+</g)
      if (result.length >= 6 && result.length < 8) {
        result.splice(-4)
      } else if (result.length >= 8) {
        result.splice(-5)
      } else {
        result.splice(-1)
      }
      inputValue.value = result.map((item) => item.match(/[\u4e00-\u9fa5]+/)[0])
      drawBounds()
      infoWindow.setContent(e.target.content)
      infoWindow.open(mapRef, e.target.getPosition())
    })
    markersList.push(marker)
  })
}

// 地图缩放 zoom 改变
const mapZoom = () => {
  const currentZoom = mapRef.getZoom()
  if (beforeZoom > changeSize && currentZoom > changeSize && searchZoom.value > changeSize) {
    return
  }
  if (
    beforeZoom <= changeSize &&
    currentZoom <= changeSize &&
    searchZoom.value <= changeSize
  ) {
    return
  }
  removeMarkers()
  closeInfo()
  if (currentZoom <= changeSize) {
    addPoint(1)
  } else {
    addPoint(9)
  }
  searchZoom.value = mapRef.getZoom()
}

// 移除地图 mark
const removeMarkers = () => {
  mapRef && mapRef.remove(markersList)
}